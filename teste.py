#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as ml

#pocos = np.fromfile( "pocos.txt" , sep='\n')
pocos = open( "teste.xyz", "r")

x = []
y = []
z = []

for poco in pocos:
	a = poco.split("\t")[0]
	a = float(a)
	x.append(a)
	b = poco.split("\t")[1]
	b = float(b)
	y.append(b)
	c = poco.split("\t")[2]
	c = float(c)
	z.append(c)


X, Y = np.meshgrid(x, y)

#print X
#print Y

zi = ml.griddata(x, y, z, x, y, interp='linear')

#print zi
grafico = plt.figure()

cs = plt.contour(x, y, zi)
plt.clabel(cs)
plt.pcolormesh(x, y, zi, cmap = plt.get_cmap('rainbow'))
plt.colorbar()

plt.show()

